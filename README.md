# play-with-grain

## WAGI: WebAssembly Gateway Interface
> - WAGI is the easiest way to get started writing WebAssembly microservices and web apps.
> - DeisLabs @deislabs | Open Source from Microsoft @Azure

- https://github.com/deislabs/wagi
- https://deislabs.io/
- https://deislabs.io/posts/wagi-updates/
- https://deislabs.io/posts/introducing-wagi-easiest-way-to-build-webassembly-microservices/
