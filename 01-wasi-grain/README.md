# Grain, un nouveau langage (fonctionnel) qui compile en wasm

https://github.com/grain-lang/grain
https://grain-lang.org/

```bash
grain compile hello.gr 
grain run helloo.gr.wasm
grain run helloo.gr.wasm bob morane
```