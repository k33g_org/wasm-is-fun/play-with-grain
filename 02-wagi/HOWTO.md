- https://github.com/deislabs/wagi
- https://github.com/deislabs/wagi-examples

```bash
wget https://github.com/deislabs/wagi/releases/download/v0.4.0/wagi-v0.4.0-linux-amd64.tar.gz
tar -zxf wagi-v0.4.0-linux-amd64.tar.gz
rm wagi-v0.4.0-linux-amd64.tar.gz
```

```bash
cd modules
grain compile hello.gr 
```

```bash
./wagi -c ./modules.toml
curl $(gp url 3000)/hello-grain
curl $(gp url 3000)/hello-grain?message=hello
```

